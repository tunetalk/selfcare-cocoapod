//
//  CurrentAddrViewController.h
//  TuneTalk
//
//  Created by Rex on 18/01/2018.
//  Copyright © 2018 Nexstream. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XLForm/XLForm.h>
#import "XLFormCustomCell.h"
#import "GeoTableViewController.h"
#import "SelfRegistration.h"
#import "WebService.h"
#import "LocalizedString.h"
#import "Constants.h"
#import "UIViewController+Other.h"
#import "LiveDetCoachingViewController.h"

//#import "LivenessDetResultViewController.h"

@interface CurrentAddrViewController : XLFormViewController

@property BOOL isMalaysia;
@property BOOL isMyTentera;
@property NSString* apiKey;
@property (nonatomic, copy, nullable) void (^completion)(NSInteger, NSString*, NSString*,NSString*);

@end
