//
//  CapturePhotoViewController.h
//  TTSelfcareSDK
//
//  Created by james ong on 27/04/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CapturePhotoViewController : UIViewController
@property (nonatomic, copy, nullable) void (^completion)(NSString*);
@end

NS_ASSUME_NONNULL_END
