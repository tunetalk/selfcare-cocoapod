//
//  EnterMobileNumberViewController.h
//  TTSelfcareSDK
//
//  Created by James Nexstream on 27/01/2022.
//  Copyright © 2022 Tunetalk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelfRegistration.h"
#import "WebService.h"
#import "LocalizedString.h"
#import "Constants.h"
#import "UIViewController+Other.h"
#import "SimReplacement.h"
#import "SimReplacementDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface EnterMobileNumberViewController : UIViewController
@property NSString* apiKey;
@property (nonatomic, copy, nullable) void (^completion)(NSInteger, NSString*, RegistrationDetail*);
@end

NS_ASSUME_NONNULL_END
