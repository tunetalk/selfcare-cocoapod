//
//  SimRegViewController.h
//  TuneTalk
//
//  Created by Rex on 15/01/2018.
//  Copyright © 2018 Nexstream. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelfRegistration.h"
#import "WebService.h"
#import "LocalizedString.h"
#import "Constants.h"
#import "UIViewController+Other.h"
#import "SelectNumberController.h"
#import "RegistrationDetail.h"

@interface SimRegViewController : UIViewController
@property NSString* donorTelco;
@property NSString* msisdn;
@property NSString* transId;
@property NSString* blockedNumber;

@property NSString* apiKey;
@property NSString* microBlinkLicenseKey;
@property NSString* registerId;
@property BOOL isSelectNumber;
@property NSString* esimIccid;
//@property NSString* applyHerogoDetails;
@property (nonatomic, copy, nullable) void (^completion)(NSInteger, NSString*, RegistrationDetail*);

@end
