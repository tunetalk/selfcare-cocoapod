//
//  WebService.h
//  TuneTalk
//
//  Created by Shah Jahan on 12/26/14.
//  Copyright (c) 2014 Nexstream. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <MMProgressHUD/MMProgressHUD.h>
#import "WebService.h"
#import "LocalizedString.h"
#import "Constants.h"
#import "NSDictionary+Other.h"
#import "SelfRegistration.h"
#import "RegistrationDetail.h"
#import "SimReplacement.h"
#import "SimReplacementDetail.h"

@protocol WebServiceDelegate;
@interface WebService : NSObject

@property NSTimer* timer;
@property (nonatomic) id <WebServiceDelegate> delegate;
typedef void(^postRequestBlock)(BOOL status);
-(void)sdkInit:(NSString*)bundleID apiKey:(NSString*)apiKey platform:(NSString*)platform;
-(void)okDocValidation:(NSString*)apiKey image:(NSString*)image idType:(NSString*)idType country:(nullable NSString*)country completion:(postRequestBlock)completion;
-(void)registerStep1:(NSString*)apiKey;
-(void)registerStep2:(NSString*)apiKey msisdn:(nullable NSString*)msisdn registerId:(NSString*)registerId simNumber:(NSString*)simNumber;
-(void)checkSimType:(NSString*)apiKey simNum:(NSString*)simNum;
-(void)getNumberFrom4Digits:(NSString*)apiKey digits:(NSString*)digits;
-(void)checkValidTunetalkNumber:(NSString*)apiKey msisdn:(NSString*)msisdn;
-(void)simReplacement:(NSString*)apiKey;
-(void)hudShouldEnd;
@end


@protocol WebServiceDelegate <NSObject>

@optional
-(void)sdkInitResponse:(BOOL)isSuccessful message:(nullable NSString*)message microBlinkLicenseKey:(nullable NSString*)microBlinkLicenseKey;
-(void)okDocValidationResponse:(NSInteger)resultCode message:(nullable NSString*)message;
-(void)registerStep1Response:(NSInteger)resultCode message:(nullable NSString*)message registerId:(nullable NSString*)registerId;
-(void)registerStep2Response:(NSInteger)resultCode message:(nullable NSString*)message regDetail:(nullable RegistrationDetail*)regDetail;
-(void)getNumberFrom4DigitsResponse:(NSArray *)numberList;
-(void)checkSimTypeResponse:(BOOL)isValidSim simNumber:(nullable NSString*)simNumber;
-(void)checkValidTunetalkNumberResponse:(NSInteger)resultCode message:(nullable NSString*)message;
-(void)simReplacementResponse:(NSInteger)resultCode message:(nullable NSString*)message simReplacementDetail:(nullable RegistrationDetail*)simReplacementDetail;

@end

