//
//  SimReplacement.h
//  TTSelfcareSDK
//
//  Created by James Nexstream on 28/01/2022.
//  Copyright © 2022 Tunetalk. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
#define kSelfRegIDTypePassport @"PASSPORT"
#define kSelfRegIDTypeNRIC @"MYKAD"
#define kSelfRegIDTypeMyTentera @"ARMY_ID"

@interface SimReplacement : NSObject
+ (instancetype)sharedInstance;
+ (void)resetSharedInstance;
+ (void)resetOCRData;
+ (void)extractFirstLastName:NSString;
+(NSMutableDictionary*)formParameters;


@property NSString* simNumber; //8960199900

@property NSString* msisdn;

//reset
@property NSString* firstName;
@property NSString* lastName;
@property NSString* idType;
@property NSString* idNumber;
@property NSString* idPhoto;
@property NSString* reason;

@end

NS_ASSUME_NONNULL_END
