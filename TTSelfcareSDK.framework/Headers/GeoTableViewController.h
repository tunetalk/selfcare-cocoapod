//
//  GeoTableViewController.h
//  TuneTalk
//
//  Created by Rex Ong on 7/29/15.
//  Copyright (c) 2015 Nexstream. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XLForm/XLForm.h>

enum {
    GeoTypeCountry = 1,
    GeoTypeState = 2,
    GeoTypeCity = 3
};

@protocol GeoTableViewControllerDelegate<NSObject>
@property NSString* stateID;
-(void)countrySelected:(NSString*)country countryCode:(NSString*)countryCode;
-(void)stateSelected:(NSString*)state stateCode:(NSString*)stateCode;
-(void)citySelected:(NSString*)city;
@end

@interface GeoTableViewController : UITableViewController<UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>
@property id<GeoTableViewControllerDelegate> delegate;
@property (strong, nonatomic) UISearchController *searchController;
@property NSInteger geoType;
@property NSString* state;
@end
