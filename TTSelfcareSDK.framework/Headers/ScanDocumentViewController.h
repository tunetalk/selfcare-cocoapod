//
//  ScanDocumentViewController.h
//  TTSelfcareSDK
//
//  Created by James Nexstream on 26/01/2022.
//

#import <UIKit/UIKit.h>
#import "SelfRegistration.h"
#import "WebService.h"
#import "LocalizedString.h"
#import "Constants.h"
#import "UIViewController+Other.h"
#import "SimReplacementDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScanDocumentViewController : UIViewController
@property NSString* apiKey;

@property (nonatomic, copy, nullable) void (^completion)(NSInteger, NSString*, RegistrationDetail*);
@end

NS_ASSUME_NONNULL_END
