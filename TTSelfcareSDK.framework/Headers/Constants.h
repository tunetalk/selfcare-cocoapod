//
//  Constants.h
//  TuneTalk
//
//  Created by Shah Jahan on 12/6/14.
//  Copyright (c) 2014 Nexstream. All rights reserved.
//

#ifndef TuneTalk_Constants_h
#define TuneTalk_Constants_h

//Precompiled OS Check
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//Get Build Date & Time
#define BUILD_DATE [NSString stringWithUTF8String:__DATE__]
#define BUILD_TIME [NSString stringWithUTF8String:__TIME__]

//Font
#define appFontRegular [UIFont systemFontOfSize:17]
#define appFontLight [UIFont systemFontOfSize:17 weight:UIFontWeightLight]
#define appFontBold19 [UIFont systemFontOfSize:19]
#define appFontBold [UIFont systemFontOfSize:17 weight:UIFontWeightBold]

//Color
#define viewBackgroundColor [UIColor groupTableViewBackgroundColor]
#define themeColor [UIColor colorWithRed:226.0/255.0 green:32.0/255.0 blue:42.0/255.0 alpha:1.0]
#define statusOrange [UIColor colorWithRed:254.0f/255.0f green:169.0f/255.0f blue:9.0f/255.0f alpha:1]
#define statusGreen [UIColor colorWithRed:73.0f/255.0f green:175.0f/255.0f blue:77.0f/255.0f alpha:1]
#define statusRed [UIColor colorWithRed:255.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1]
#define backgroundTransparent [UIColor colorWithRed:3.0f/255.0f green:3.0f/255.0f blue:3.0f/255.0f alpha:0.3]
#define lightBlue [UIColor colorWithRed:29.0f/255.0f green:175.0f/255.0f blue:236.0f/255.0f alpha:1]
#define LightDarkBlue [UIColor colorWithRed:29.0f/255.0f green:139.0f/255.0f blue:240.0f/255.0f alpha:1]
#define btnLightGray [UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1]

//Precompiled Methods
#define isBiggerThan4Inch                              [[UIScreen mainScreen] bounds].size.height>568
#define IS_PHONE  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define isIphoneX                                    [[UIScreen mainScreen] nativeBounds].size.height >= 2436 && IS_PHONE

#define SetBool(v,k)\
[[NSUserDefaults standardUserDefaults]setBool:v forKey:k];\
[[NSUserDefaults standardUserDefaults] synchronize]

#define GetBool(k)                                     [[NSUserDefaults standardUserDefaults]boolForKey:k]

#define SetObject(obj,k) [[NSUserDefaults standardUserDefaults]setObject:obj forKey:k];[[NSUserDefaults standardUserDefaults] synchronize]

#define GetObject(k)      [[NSUserDefaults standardUserDefaults]objectForKey:k]

#define SetSecretObject(obj,k) [[NSUserDefaults standardUserDefaults] setSecretObject:obj forKey:k]
#define GetSecretObject(k)     [[NSUserDefaults standardUserDefaults] secretObjectForKey:k]

#define SetCustomObject(object,key)\
NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];\
NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];\
[defaults setObject:encodedObject forKey:key];\
[defaults synchronize];\

#define GetCustomObject(key)\
NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];\
NSData *encodedObject = [defaults objectForKey:key];\
MyObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];\
return object;\

#define BAAlert(TITLE,MSG,TAG)\
UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:(TITLE) message:(MSG) delegate:nil cancelButtonTitle:[LocalizedString getLocalizedStringFor:@"OK"] otherButtonTitles:nil];\
alertView.tag          = TAG;\
[alertView show];\

#define EmptyTableView(MSG)\
({UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 250.0f)];\
UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 150, 0, 24)];\
lbl.textAlignment = NSTextAlignmentCenter;\
lbl.text = (MSG);\
lbl.font = appFontRegular;\
[lbl sizeToFit];\
lbl.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;\
[view addSubview:lbl];\
view;});

#define EnableNavAutoHide     NSLayoutConstraint *constraint = [[NSLayoutConstraint alloc]init];\
constraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.navigationController.navigationBar attribute:NSLayoutAttributeBottom multiplier:0 constant:0];\
[self setShouldScrollWhenContentFits:YES];\
[self setAutomaticallyAdjustsScrollViewInsets:YES];\
[self setExtendedLayoutIncludesOpaqueBars:YES];\
[self followScrollView:self.tableView usingTopConstraint:constraint withDelay:65]

#define SetTableViewWhiteFooter   self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero]

#define getDeviceID [UIDevice currentDevice].identifierForVendor.UUIDString
#define getOneSignalUserId GetObject(kOneSignalUserId) == nil ? getDeviceID:GetObject(kOneSignalUserId)

//Notification Name
#define NotificationPlanViewDidTapped @"NotificationPlanViewDidTapped"
#define NotificationAddDidTapped @"NotificationAddDidTapped"

//Keys
#define kSetLang            @"kSetLang"
#define kCurrentLang        @"kCurrentLang"
#define kOSLang             @"kOSLang"
#define kChangedLang        @"kChangedLang"
#define groupDefault [[NSUserDefaults alloc]initWithSuiteName:@"group.com.Tunetalk.TuneTalkPrepaid"]

//URLS
#define kSelfcareDevURL      @"http://sandbox.sl.openapi.tunetalk.net"
#define kSelfcareProURL      @"https://sl-openapi.tunetalk.net"
#define kStoredURL   @"storedURL"

//****
#define kServerURL GetObject(kStoredURL) == nil ? kSelfcareProURL : GetObject(kStoredURL)
//****

#define kPostInit                [kServerURL stringByAppendingString:@"/api/app/init"]
#define kGetPreferNumber         [kServerURL stringByAppendingString:@"/api/app/preferNumber/%@"]
#define kPostOkDocValidation     [kServerURL stringByAppendingString:@"/api/app/okayDoc/validateID"]
#define kPostRegisterStep1       [kServerURL stringByAppendingString:@"/api/app/register/step1"]
#define kPostRegisterStep2       [kServerURL stringByAppendingString:@"/api/app/register/step2"]
#define kGetCheckSIM             [kServerURL stringByAppendingString:@"/api/app/simNumber/%@"]
#define kGetTelcoList            [kServerURL stringByAppendingString:@"/api/app/telco"]
#define kGetIsValidTunetalkNumber         [kServerURL stringByAppendingString:@"/api/app/isValidMsisdn/%@"]
#define kPostSimReplacement         [kServerURL stringByAppendingString:@"/api/app/simReplacement"]

#endif
