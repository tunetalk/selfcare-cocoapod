//
//  ScanSIMViewController.h
//  TTSelfcareSDK
//
//  Created by James Nexstream on 26/01/2022.
//  Copyright © 2022 Tunetalk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelfRegistration.h"
#import "WebService.h"
#import "LocalizedString.h"
#import "Constants.h"
#import "UIViewController+Other.h"
#import "SimReplacementDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScanSIMViewController : UIViewController
@property NSString* custImage;
@property NSString* custTitle;
@property NSString* custDesc1;
@property NSString* custDesc2;
@property NSString* custProceedTitle;

@property NSString* apiKey;
@property NSString* esimIccid;

@property (nonatomic, copy, nullable) void (^completion)(NSInteger, NSString*, RegistrationDetail*);
@end

NS_ASSUME_NONNULL_END
