//
//  LiveDetCoachingViewController.h
//  TuneTalk
//
//  Created by Rex on 17/01/2018.
//  Copyright © 2018 Nexstream. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Masonry/Masonry.h>
//#import <MGLivenessDetection/MGLivenessDetection.h>
#import "CurrentAddrViewController.h"
#import "SelfRegistration.h"

@interface LiveDetCoachingViewController : UIViewController

@property NSString* apiKey;
@property (nonatomic, copy, nullable) void (^completion)(NSInteger, NSString*, NSString*,NSString*);

@end
