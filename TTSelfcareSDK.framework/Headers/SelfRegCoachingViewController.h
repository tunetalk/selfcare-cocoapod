//
//  SelfRegCoachingViewController.h
//  TuneTalk
//
//  Created by Rex on 16/01/2018.
//  Copyright © 2018 Nexstream. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelfRegistration.h"
#import "WebService.h"
#import "LocalizedString.h"
#import "Constants.h"
#import "UIViewController+Other.h"
#import "CurrentAddrViewController.h"
#import "LiveDetCoachingViewController.h"

@interface SelfRegCoachingViewController : UIViewController

@property NSString* apiKey;
@property NSString* microBlinkLicenseKey;
@property (nonatomic, copy, nullable) void (^completion)(NSInteger, NSString*, NSString* , NSString*);

@end
