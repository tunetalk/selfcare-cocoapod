//
//  SelfRegistration.h
//  TuneTalk
//
//  Created by Rex on 23/01/2018.
//  Copyright © 2018 Nexstream. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSelfRegIDTypePassport @"PASSPORT"
#define kSelfRegIDTypeNRIC @"MYKAD"
#define kSelfRegIDTypeMyTentera @"ARMY_ID"

@interface SelfRegistration : NSObject
+ (instancetype)sharedInstance;
+ (void)resetSharedInstance;
+ (void)resetOCRData;
+ (void)extractFirstLastName;
+(NSMutableDictionary*)formParameters;
@property BOOL isPortIn;
@property UIImage* faceImg;
@property NSString* validateId;
@property int retryCounter;
@property BOOL isLivenessDetectionSuccess;
@property NSInteger confidenceLevel;
@property NSString* transId;
@property NSString* blockedNumber;
@property NSString* simNumber; //8960199900
@property NSString* date; //"yyyy-MM-dd HH:mm:ss"
@property NSString* fullName;
@property NSString* firstName;
@property NSString* lastName;
@property NSString* idType;
@property NSString* idNumber;
@property NSMutableArray* photos;
@property NSString* latitude;
@property NSString* longitude;
@property NSString* gender;
@property NSString* nationality;
@property NSString* dob;
@property NSString* address;
@property NSString* city;
@property NSString* state;
@property NSString* country;
@property NSString* postCode;
@property NSString* donorTelco;
@property NSString* msisdn;
@property BOOL isMalaysia;
@property BOOL isNumberSim;
@property BOOL isManualEntry;
@property NSString* idPhoto;
@property NSString* selfie;
@property NSString* registedNumber;
@property NSString* applyHerogoDetails;

@end
