//
//  SelfCareRegistration.h
//  SelfCareRegistration
//
//  Created by AshtonTam on 25/10/2018.
//  Copyright © 2018 Tunetalk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

//! Project version number for SelfCareRegistration.
FOUNDATION_EXPORT double SelfCareRegistrationVersionNumber;

//! Project version string for SelfCareRegistration.
FOUNDATION_EXPORT const unsigned char SelfCareRegistrationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SelfCareRegistration/PublicHeader.h>
