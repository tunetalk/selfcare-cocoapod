//
//  SimReplacementDetail.h
//  TTSelfcareSDK
//
//  Created by James Nexstream on 25/02/2022.
//  Copyright © 2022 Tunetalk. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SimReplacementDetail : NSObject
@property NSString* simNumber; //8960199900
@property NSString* msisdn;
@property NSString* firstName;
@property NSString* lastName;
@property NSString* idType;
@property NSString* idNumber;
@end

NS_ASSUME_NONNULL_END
