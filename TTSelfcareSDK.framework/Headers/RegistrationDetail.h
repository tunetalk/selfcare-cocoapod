//
//  RegistrationDetail.h
//  TTSelfcareSDK
//
//  Created by AshtonTam on 05/11/2018.
//  Copyright © 2018 Tunetalk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegistrationDetail : NSObject
    
@property NSString* firstName;
@property NSString* lastName;
@property NSString* idType;
@property NSString* idNumber;
@property NSString* dob;
@property NSString* address;
@property NSString* city;
@property NSString* state;
@property NSString* postCode;
@property NSString* country;
@property NSString* msisdn;
@property NSString* gender;
@property NSString* iccid;
@property NSString* oldIccid;
@property NSString* temporaryMsisdn;
@property NSString* herogoValidateIccidReqBody;
@property NSMutableArray* photos;

@end
