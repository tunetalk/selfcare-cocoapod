//
//  NSDictionary.h
//  TuneTalk
//
//  Created by AshtonTam on 13/07/2018.
//  Copyright © 2018 Nexstream. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Other)

-(id)getObjectWith:(id)Key failureValue:(id)value;

@end
